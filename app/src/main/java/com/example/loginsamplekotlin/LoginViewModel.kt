package com.example.loginsamplekotlin

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.example.loginsamplekotlin.Interface.MessageCallback
import com.example.loginsamplekotlin.Interface.ResponseCallback
import com.example.loginsamplekotlin.Data.LoginModel
import com.example.loginsamplekotlin.Data.LoginResponse

class LoginViewModel(
    application: Application, var msgCallback: MessageCallback
) : AndroidViewModel(application), ResponseCallback {

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val isLoading = MutableLiveData<Int>()

    var loginInput = MutableLiveData<LoginModel>()

    init {
        isLoading.value = View.GONE
    }

    override fun onSuccess(data: LoginResponse) {
        Log.v("MainActivity", "viewmodel response: "+data.code)

        if (data.code.equals("0")){
            msgCallback.successMessage(data.message!!)
        }
        else{
            msgCallback.successMessage(data.message!!)
        }
    }

    override fun OnFailure(data: LoginResponse) {
    }

    fun getLogin(){
        var repository = LoginRepository(this)
        repository.getLogin(loginInput.value?.email,loginInput.value?.password, isLoading)

    }

    fun OnClick(){
        if (email.value!=null && email.value!!.length!=0){

            if (password.value!=null && password.value!!.length!=0){

                var loginModel =
                    LoginModel(email.value!!, password.value!!)
                loginInput?.value = loginModel
                getLogin()
            }
            else{
                msgCallback.failureMessage("Please enter your password")
            }
        }
        else{
            msgCallback.failureMessage("Please enter your email")
        }

    }

    class LoginModelFactory(var application: Application, var callback: MessageCallback) :ViewModelProvider.Factory{
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(application, callback) as T
        }

    }

}

