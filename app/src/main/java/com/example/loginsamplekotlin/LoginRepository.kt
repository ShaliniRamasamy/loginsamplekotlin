package com.example.loginsamplekotlin

import android.util.Log
import androidx.lifecycle.MutableLiveData
import android.view.View
import com.example.loginsamplekotlin.Data.LoginResponse
import com.example.loginsamplekotlin.Interface.Api
import com.example.loginsamplekotlin.Interface.ResponseCallback
import com.example.loginsamplekotlin.Network.ApiServiceBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginRepository(var callback: ResponseCallback) {

    var loginResponse = MutableLiveData<LoginResponse>()

    fun getLogin(email: String?, password:String?, isLoading:MutableLiveData<Int>):MutableLiveData<LoginResponse>{

        isLoading.value = View.VISIBLE
        val jsonObject = JsonObject()

        jsonObject.addProperty("Loginid", email)
        jsonObject.addProperty("Password", password)

        Log.d("Login", "input: $jsonObject")

        val apiService = ApiServiceBuilder.createService(Api::class.java)

        val call = apiService.getLogin(jsonObject)

        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                isLoading.value = View.GONE
                loginResponse.value = response.body()
                response?.body()?.let {
                    if (response.isSuccessful){
                        callback?.onSuccess(it)

                    }
                    else{

                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                isLoading.value = View.GONE
            }
        })

        return loginResponse
    }
}