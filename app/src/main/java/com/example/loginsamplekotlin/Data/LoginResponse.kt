package com.example.loginsamplekotlin.Data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse{

    @SerializedName("Code")
    @Expose
    var code: String? = null
    @SerializedName("Message")
    @Expose
    var message:String? = null
    @SerializedName("Loginid")
    @Expose
    var loginId: String? = null

}