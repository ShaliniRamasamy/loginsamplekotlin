package com.example.loginsamplekotlin.Ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.loginsamplekotlin.Interface.MessageCallback
import com.example.loginsamplekotlin.LoginViewModel
import com.example.loginsamplekotlin.R
import com.example.loginsamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), MessageCallback {

    var loginViewModel: LoginViewModel? = null
    var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var loginModelFactory =
            LoginViewModel.LoginModelFactory(application, this)
        loginViewModel = ViewModelProviders.of(this, loginModelFactory).get(LoginViewModel::class.java)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )
        binding?.loginModel = loginViewModel
        binding?.setLifecycleOwner(this)

    }

    override fun successMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun failureMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
