package com.example.loginsamplekotlin.Interface

import com.example.loginsamplekotlin.Data.LoginResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST



interface Api {

    //login
    @Headers("Accept: application/json", "Content-Type: application/json")
    @POST("Login")
    abstract fun getLogin(@Body jsonObject: JsonObject): Call<LoginResponse>

}