package com.example.loginsamplekotlin.Interface

import com.example.loginsamplekotlin.Data.LoginResponse

interface ResponseCallback{
    fun onSuccess(data: LoginResponse)
    fun OnFailure(data: LoginResponse)
}