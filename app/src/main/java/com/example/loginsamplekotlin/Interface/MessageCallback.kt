package com.example.loginsamplekotlin.Interface

interface MessageCallback{
    fun successMessage(msg: String)
    fun failureMessage(msg: String)
}